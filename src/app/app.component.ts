import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms'
import axios from 'axios'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'my-app'

  contries = []

  guyForm = new FormGroup({
    Name: new FormControl(
      '',
      [Validators.required, Validators.pattern(/^[A-Za-z]+$/)]
    ),
    LastName: new FormControl(
      '',
      [Validators.required, Validators.pattern(/^[A-Za-z]+$/)]
    ),
    Email: new FormControl(
      '',
      [Validators.required, Validators.pattern(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/)]
    ),
    Country: new FormControl(
      'Afghanistan',
      [Validators.required]
    ),
    Gender: new FormControl(
      'female'
    ),
    Telephone: new FormControl(
      '',
      [Validators.required]
    )
  })

  async ngOnInit() { 
     const response = await axios.get('https://restcountries.eu/rest/v2/all')
     this.contries = response.data.map((row) => row.name)
     this.guyForm.patchValue({gender: 'male'})
   }

  get guyName() {
    return this.guyForm.get('Name')
  }

  get guyLastName() {
    return this.guyForm.get('LastName')
  }

  get guyEmail() {
    return this.guyForm.get('Email')
  }

  get guyPhone() {
    return this.guyForm.get('Telephone')
  }

  get form(){

    return this.guyForm.controls;

  }

  submit(){

    console.log(this.guyForm.value);

  }
}
