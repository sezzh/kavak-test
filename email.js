export default function emailUnicos(emails) {
  const cleanedEmails = emails.map(cleanEmails)
  return filterUniqueEmails(cleanedEmails, (email) => email.emailsToSend)
    .map((email) => email.emailsToSend)
}

function cleanEmails(email) {
  console.log(email)
  const cleanedEmail = {}
  cleanedEmail.prefixEmail = getPrefixEmail(email)
  cleanedEmail.domain = getDomainEmail(email)
  cleanedEmail.emailsToSend = `${cleanedEmail.prefixEmail}${cleanedEmail.domain}`
  return cleanedEmail
}

function getPrefixEmail(email) {
  const indexOfPlusSign = email.indexOf('+')
  const emailPrefixClean = email.substring(0, indexOfPlusSign).replace(/\./g, '')
  return emailPrefixClean

}

function getDomainEmail(email) {
  const indexOfAt = email.indexOf('@')
  return email.substring(indexOfAt, email.length)
}

function filterUniqueEmails(cleanedEmails, key) {
  let seen = new Set();
  return cleanedEmails.filter(item => {
      let k = key(item);
      return seen.has(k) ? false : seen.add(k);
  });
}